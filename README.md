# mycelial.technology

Personal website of glyph.

Handwritten HTML and CSS organised into templates ([Tera](https://tera.netlify.app/)), served with [Rocket](https://rocket.rs/) and proxied through Nginx.

Includes a custom RSS generator (`src/bin/generate_rss.rs`).

## License

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
