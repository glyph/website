{% extends "nav" %}
{% block title %}mycelial technology | reading list{% endblock title %}
{% block content %}
    <h2>Mycology Literature Reading List</h2>
    <h3>2021</h3>
    <p>I'm aiming to read 100 mycology journal articles and textbook chapters this year. Any entries marked with a * have been read and discussed as part of the Hyphal Fusion Reading Group.</p>
    <ol>
      <li>Hiscox J, O'Leary J, Boddy L (2018). Fungus wars: basidiomycete battles in wood decay. <i>Studies in Mycology</i> <b>89</b>: 117–124. *</li>
      <li>Crowther TW, Boddy L, Maynard DS (2018). The use of artificial media in fungal ecology. <i>Fungal Ecology</i> <b>32</b>: 87–91.</li>
      <li>Parfitt D, Hunt J, Dockrell D, <i>et al.</i> (2010). Do all trees carry the seeds of their own destruction? PCR reveals numerous wood decay fungi latently present in sapwood of a wide range of angiosperm trees. <i>Fungal Ecology</i>, <b>3</b>: 338–346.</li>
      <li>Boddy L, Crockatt ME, Ainsworth AM (2011). Ecology of Hericium cirrhatum, H. coralloides and H. erinaceus in the UK. <i>Fungal Ecology</i>, <b>4</b>(2): 163–173.</li>
      <li>Heaton L, Obara B, Grau V, <i>et al.</i> (2012). Analysis of fungal networks. <i>Fungal Biology Reviews</i>, <b>26</b>(1): 12-29.</i></li>
      <li>Mueller U, Kardish M, Ishak H, <i>et al.</i> (2018). Phylogenetic patterns of ant–fungus associations indicate that farming strategies, not only a superior fungal cultivar, explain the ecological success of leafcutter ants. <i>Molecular Ecology</i>, <b>27</b>(10): 2414-2434.</li>
      <li>Yang D, Liang J, Wang Y, <i>et al.</i> (2016). Tea waste: an effective and economic substrate for oyster mushroom cultivation. <i>Journal of the Science of Food and Agriculture</i>, <b>96</b>(2), 680-684.</li>
      <li>Richter DL, Dixon TG, Smith JK (2016). Revival of saprotrophic and mycorrhizal basidiomycete cultures after 30 years in cold storage in sterile water. <i>Canadian Journal of Microbiology</i>, <b>62</b>(11), 932-937.</li>
      <li>Schwartz MW, Hoeksema JD, Gehring CA, <i>et al.</i> (2006). The promise and the potential consequences of the global transport of mycorrhizal fungal inoculum. <i>Ecology Letters</i>, <b>9</b>(5), 501-515.</li>
      <li>Kües U & Liu Y (2000). Fruiting body production in basidiomycetes. <i>Applied Microbiology and Biotechnology</i>, <b>54</b>(2), 141-152.</li>
      <li>Jusino MA, Lindner DL, Banik MT, <i>et al.</i> (2016). Experimental evidence of a symbiosis between red-cockaded woodpeckers and fungi. <i>Proceedings of the Royal Society B: Biological Sciences</i>, <b>283</b>(1827).
      <li>Garibay-Orijel R, Ramírez-Terrazo A & Ordaz-Velázquez M. (2012). Women care about local knowledge, experiences from ethnomycology. <i>Journal of Ethnobiology and Ethnomedicine</i>, <b>8</b>(1), 1-13.</li>
      <li>Raudabaugh DB, Matheny PB, Hughes KW, <i>et al.</i> (2020). Where are they hiding? Testing the body snatchers hypothesis in pyrophilous fungi. <i>Fungal Ecology</i>, <b>43</b>, 100870.</li>
      <li>Ingham CJ, Kalisman O, Finkelshtein A, Ben-Jacob E (2011). Mutually facilitated dispersal between the nonmotile fungus Aspergillus fumigatus and the swarming bacterium Paenibacillus vortex. <i>Proceedings of the National Academy of Sciences</i>, <b>108</b>(49):19731-6.</li>
    </ol>
    <hr>
{%- endblock %}
